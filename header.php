<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php wp_title( '-', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
<script src="https://use.typekit.net/ruu1fsh.js"></script>
<script>try{Typekit.load({ async: true });}catch(e){}</script>
</head>
<body <?php body_class(); ?>>

<div id="page" class="hfeed site">

<?php if(is_front_page()) { 
    
    global $wp_query;
    $postid = $wp_query->post->ID;
    
    ?>
    
    <header class="homepage-header">
        <div class="message">
            <div class="site-branding">
                <h1 class="logo">
                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home" title="Perma">
                        <img src="<?php echo get_bloginfo('template_url') ?>/assets/img/perma-logo.svg" class="header-logo" title="<?php bloginfo('name'); ?>">
                    </a>
                </h1>
            </div>
            <div class="header-content">
                <p><?php the_field('header_message', $postid); ?></p>
                <div class="header-button"><a class="button" href="#wedding-form">Book Now</a></div>
            </div>
        </div>
        
        <div class="perma-picture"></div>
    </header>
    
<?php } else { ?>
    
    <header class="site-header">
        <div class="container">
            <div class="site-branding">
                <h1 class="logo">
                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home" title="Perma">
                        <img src="<?php echo get_bloginfo('template_url') ?>/assets/img/perma-logo.svg" class="header-logo" title="<?php bloginfo('name'); ?>">
                    </a>
                </h1>
            </div>
            <!-- <nav id="site-navigation" class="site-navigation">
                <div id="responsive-menu"><?php// wp_nav_menu(array('theme_location' => 'header', 'menu_id' => 'menu-header', 'menu_class' => 'header-navigation')); ?></div>
            </nav> -->
        </div>
    </header>
    
<?php } ?>

<div class="wrap-main">
    <div class="page-container">
