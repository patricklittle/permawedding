<?php /* Template Name: Homepage */ ?>

<?php get_header(); ?>
<section id="primary" class="content-area">
    <main id="main" class="site-main">
        <?php if ( have_posts() ) {
          while ( have_posts() ) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class('section-article'); ?> role="article">
              
              <div class="page-content">
                  <?php the_field('latest_release'); ?>
                  <hr>
                  <div id="wedding-form">
                      <?php the_field('wedding_form'); ?>
                  </div>
              </div>
              
            </article>
          <?php endwhile;
        } ?>
    </main>
</section>

<?php // get_sidebar();?>
<?php get_footer(); ?>