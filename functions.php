<?php // ==== FUNCTIONS ==== //

// Load configuration defaults for this theme; anything not set in the custom configuration (above) will be set here
defined( 'permaweddings_SCRIPTS_PAGELOAD' )       || define( 'permaweddings_SCRIPTS_PAGELOAD', true );

// An example of how to manage loading front-end assets (scripts, styles, and fonts)
require_once( trailingslashit( get_stylesheet_directory() ) . 'inc/assets.php' );

// Required to demonstrate WP AJAX Page Loader (as WordPress doesn't ship with even simple post navigation functions)
require_once( trailingslashit( get_stylesheet_directory() ) . 'inc/navigation.php' );

// Only the bare minimum to get the theme up and running
function permaweddings_setup() {

  // HTML5 support; mainly here to get rid of some nasty default styling that WordPress used to inject
  add_theme_support( 'html5', array( 'search-form', 'gallery' ) );

  // Automatic feed links
  add_theme_support( 'automatic-feed-links' );

  add_theme_support( 'title-tag' );

  // $content_width limits the size of the largest image size available via the media uploader
  // It should be set once and left alone apart from that; don't do anything fancy with it; it is part of WordPress core
  global $content_width;
  $content_width = 960;

  // Register header and footer menus
  register_nav_menu( 'header', __( 'Header Menu', 'permaweddings' ) );
  register_nav_menu( 'footer', __( 'Footer Menu', 'permaweddings' ) );
}
add_action( 'after_setup_theme', 'permaweddings_setup', 11 );

// Sidebar declaration
function permaweddings_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Main sidebar', 'permaweddings' ),
    'id'            => 'sidebar-main',
    'description'   => __( 'Appears to the right side of most posts and pages.', 'permaweddings' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>'
  ) );
}
add_action( 'widgets_init', 'permaweddings_widgets_init' );

add_theme_support( 'post-thumbnails' );

function wpdocs_excerpt_more( $more ) {
    return sprintf( '...<a class="read-more" href="%1$s">%2$s</a>',
        get_permalink( get_the_ID() ),
        __( 'Read More', 'permaweddings' )
    );
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

// Wrap iframes and Embeds
function responsive_embed($content) {
    // match any iframes
    $pattern = '~<iframe.*</iframe>|<embed.*</embed>~';
    preg_match_all($pattern, $content, $matches);

    foreach ($matches[0] as $match) {
        // wrap matched iframe with div
        $wrappedframe = '<div class="responsive-embed">' . $match . '</div>';

        //replace original iframe with new in content
        $content = str_replace($match, $wrappedframe, $content);
    }

    return $content;    
}
add_filter('the_content', 'responsive_embed');
add_filter('latest_release', 'responsive_embed');

// Custom Post Types
include_once('inc/custom-post-types.php');

// Custom Images
include_once('inc/image-sizes.php');
